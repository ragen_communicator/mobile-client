package pl.pmajewski.ragen.clientAlpha.stomp.client;

public interface StompSubscription {
	
	void onMessage(Object body);
}
