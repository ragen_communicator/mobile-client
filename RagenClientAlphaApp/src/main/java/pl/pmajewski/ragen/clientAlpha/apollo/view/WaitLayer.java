package pl.pmajewski.ragen.clientAlpha.apollo.view;

import com.gluonhq.charm.glisten.application.GlassPane;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.layout.Layer;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import pl.pmajewski.ragen.clientAlpha.GlobalProperties;
import pl.pmajewski.ragen.clientAlpha.storage.ConnectorStorage;

public class WaitLayer extends Layer {

    private ConnectorStorage connectorStorage = ConnectorStorage.getInstance();

    private final Node root;
    private final double size = 150;
    final GlassPane glassPane = MobileApplication.getInstance().getGlassPane();

    public WaitLayer() {
        setAutoHide(false);
        setBackgroundFade(0.5);
        root = new StackPane(new Label("Wait for participant"), getCancelBtn());
        root.setStyle("-fx-background-color: white;");
        getChildren().add(root);
    }

    public Button getCancelBtn() {
        Button btn = new Button("Cancel");
        btn.setOnAction(e -> {
            connectorStorage.getClient().send("/app/apollo/participant/cancel", "");
            MobileApplication.getInstance().switchView(GlobalProperties.PARTICIPANT_SELECTOR_VIEW);
            MobileApplication.getInstance().removeViewFactory(GlobalProperties.RANDOM_CONVERSATION_VIEW);
            this.hide();
        });
        return btn;
    }

    @Override
    public void layoutChildren() {
        super.layoutChildren();
        root.setVisible(isShowing());
        if (!isShowing()) {
            return;
        }
        root.resize(size, size);
        resizeRelocate((glassPane.getWidth() - size)/2, (glassPane.getHeight()- size)/2, size, size);
    }
}
