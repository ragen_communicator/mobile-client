package pl.pmajewski.ragen.clientAlpha.stomp.client.exception;

public class DestinationSubscribedException extends StompClientException {

	public DestinationSubscribedException() {
		super();
	}

	public DestinationSubscribedException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public DestinationSubscribedException(String message, Throwable cause) {
		super(message, cause);
	}

	public DestinationSubscribedException(String message) {
		super(message);
	}

	public DestinationSubscribedException(Throwable cause) {
		super(cause);
	}
}
