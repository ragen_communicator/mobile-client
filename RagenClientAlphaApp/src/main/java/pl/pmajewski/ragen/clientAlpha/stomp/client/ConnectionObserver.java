package pl.pmajewski.ragen.clientAlpha.stomp.client;

public interface ConnectionObserver {
	
	void connectionOpened();
	
	void connectionClosed();
	
	Boolean isConnected();
}
