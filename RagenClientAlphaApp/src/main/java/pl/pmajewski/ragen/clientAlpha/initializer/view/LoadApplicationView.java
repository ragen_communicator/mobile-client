package pl.pmajewski.ragen.clientAlpha.initializer.view;

import com.gluonhq.charm.down.Services;
import com.gluonhq.charm.down.plugins.DeviceService;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.mvc.View;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import pl.pmajewski.ragen.clientAlpha.GlobalProperties;
import pl.pmajewski.ragen.clientAlpha.initializer.pojo.DeviceConnectAuthorizationRequestPojo;
import pl.pmajewski.ragen.clientAlpha.initializer.pojo.DeviceConnectAuthorizationResponsePojo;
import pl.pmajewski.ragen.clientAlpha.storage.ConnectorStorage;
import pl.pmajewski.ragen.clientAlpha.storage.DeviceAuthorizationStorage;

import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicReference;

public class LoadApplicationView extends View {

    private ExecutorService executor = Executors.newSingleThreadExecutor();
    private ConnectorStorage connectorStorage = ConnectorStorage.getInstance();

    public LoadApplicationView() {
        MobileApplication.getInstance().getAppBar().setVisible(false);
        GridPane panel = new GridPane();
        Label infoLbl = new Label();
        infoLbl.setText("Waiting for connection");
        panel.add(infoLbl, 1, 1);

        setCenter(panel);

        executor.submit(new WebsocketInitializer());
        executor.submit(new ConnectionInitializer());
        executor.submit(new InitializeRequest());
    }

    public String getDeviceUUID() {
        AtomicReference<String> uuid = new AtomicReference<>();
        Services.get(DeviceService.class).ifPresent(i -> {
            uuid.set(i.getUuid());
        });

        if(uuid.get() == null) {
            return "JavaGeneratedUUID="+UUID.randomUUID().toString();
        } else {
            return uuid.get();
        }
    }

    private class WebsocketInitializer implements Callable<Void> {

        @Override
        public Void call() throws Exception {
            connectorStorage.getClient().connect();
            int counter = 0;
            do {
                System.out.println("WebsocketInitializer -> "+counter++);
                Thread.sleep(250);
            } while (!connectorStorage.getClient().isConnected());


            return null;
        }
    }

    private class ConnectionInitializer implements Callable<Void> {

        @Override
        public Void call() throws Exception {
            connectorStorage.getClient().subscribe("/user/queue/initializer/authentication", DeviceConnectAuthorizationResponsePojo.class, payload -> {
                DeviceConnectAuthorizationResponsePojo response = (DeviceConnectAuthorizationResponsePojo) payload;
                System.out.println("/user/queue/initializer/authentication -> payload:\n"+response);
                DeviceAuthorizationStorage.getInstance().setId(response.getId());

                Platform.runLater(() -> {
                    MobileApplication.getInstance().getAppBar().setVisible(false);
                    MobileApplication.getInstance().switchView(GlobalProperties.MAIN_MENU_VIEW);
                });
            });
            return null;
        }
    }

    private class InitializeRequest implements Callable<Void> {

        @Override
        public Void call() throws Exception {
            String uuid = getDeviceUUID();
            connectorStorage.getClient().send("/app/initializer", new DeviceConnectAuthorizationRequestPojo(uuid));
            return null;
        }
    }
}