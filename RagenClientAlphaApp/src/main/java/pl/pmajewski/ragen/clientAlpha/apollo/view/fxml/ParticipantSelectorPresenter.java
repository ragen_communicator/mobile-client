package pl.pmajewski.ragen.clientAlpha.apollo.view.fxml;

import com.gluonhq.charm.glisten.animation.BounceInRightTransition;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.mvc.View;
import javafx.fxml.FXML;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import pl.pmajewski.ragen.clientAlpha.GlobalProperties;
import pl.pmajewski.ragen.clientAlpha.apollo.storage.ParticipantStorage;
import pl.pmajewski.ragen.clientAlpha.utils.Sex;

public class ParticipantSelectorPresenter {

    private ParticipantStorage participantStorage = ParticipantStorage.getInstance();

    private ToggleGroup userDetails = new ToggleGroup();
    private ToggleGroup preferences = new ToggleGroup();

    @FXML
    private View root;
    @FXML
    private RadioButton userMaleRadio;
    @FXML
    private RadioButton userFemaleRadio;
    @FXML
    private RadioButton preferMaleRadio;
    @FXML
    private RadioButton preferFemaleRadio;

    public void initialize() {
        root.showTransitionFactoryProperty().setValue(t -> {
            return new BounceInRightTransition(MobileApplication.getInstance().getView());
        });

        root.setOnShowing(e -> {
            root.getApplication().getAppBar().setVisible(false);
        });

        initGroups();
    }

    private void initGroups() {
        userMaleRadio.setToggleGroup(userDetails);
        userFemaleRadio.setToggleGroup(userDetails);
        preferMaleRadio.setToggleGroup(preferences);
        preferFemaleRadio.setToggleGroup(preferences);

        userMaleRadio.setUserData("MALE");
        userFemaleRadio.setUserData("FEMALE");
        preferMaleRadio.setUserData("MALE");
        preferFemaleRadio.setUserData("FEMALE");
    }

    public void search() {
        participantStorage.setUserSex(Sex.valueOf((String)userDetails.getSelectedToggle().getUserData()));
        participantStorage.setPreferedSex(Sex.valueOf((String)preferences.getSelectedToggle().getUserData()));

        MobileApplication.getInstance().addViewFactory(GlobalProperties.RANDOM_CONVERSATION_VIEW, () -> new ApolloConversationView().getView());
        MobileApplication.getInstance().switchView(GlobalProperties.RANDOM_CONVERSATION_VIEW);
    }
}
