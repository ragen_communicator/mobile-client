package pl.pmajewski.ragen.clientAlpha;

import com.gluonhq.charm.glisten.application.MobileApplication;

public class GlobalProperties {

    private static enum AppInstance { MOBILE, DESKTOP}

    private static AppInstance appInstance = AppInstance.MOBILE;

    public static String LOAD_APP_VIEW;
    public static final String MAIN_MENU_VIEW = "MAIN_MENU_VIEW";
    public static final String RANDOM_CONVERSATION_VIEW = "RANDOM_CONVERSATION_VIEW";
    public static final String PARTICIPANT_SELECTOR_VIEW = "PARTICIPANT_SELECTOR_VIEW";

    public static Double getScreenWidth() {
        if(AppInstance.DESKTOP.equals(appInstance)) {
            return  getEstimatedDesktopWidth();
        } else {
            return MobileApplication.getInstance().getScreenWidth();
        }
    }

    public static Double getScreenHeight() {
        if(AppInstance.DESKTOP.equals(appInstance)) {
            return getEstimatedDesktopHeight();
        } else {
            return MobileApplication.getInstance().getScreenHeight();
        }
    }

    private static Double getEstimatedDesktopWidth() {
        return 333d;
    }

    private static Double getEstimatedDesktopHeight() {
        return 600d;
    }
}
