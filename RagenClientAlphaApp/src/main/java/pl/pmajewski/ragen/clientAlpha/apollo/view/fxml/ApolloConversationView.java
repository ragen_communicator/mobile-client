package pl.pmajewski.ragen.clientAlpha.apollo.view.fxml;

import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.mvc.View;
import javafx.fxml.FXMLLoader;

import java.io.IOException;

public class ApolloConversationView {

    private View view;


    public ApolloConversationView() {
        try {
            MobileApplication.getInstance().getAppBar().setVisible(false);
            view = FXMLLoader.load(ApolloConversationView.class.getResource("ApolloConversation.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public View getView() {
        return view;
    }
}
