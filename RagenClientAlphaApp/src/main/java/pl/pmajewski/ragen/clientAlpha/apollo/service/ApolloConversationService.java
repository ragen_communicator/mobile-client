package pl.pmajewski.ragen.clientAlpha.apollo.service;

import com.google.gson.reflect.TypeToken;
import javafx.application.Platform;
import javafx.scene.control.Button;
import pl.pmajewski.ragen.clientAlpha.apollo.ConversationState;
import pl.pmajewski.ragen.clientAlpha.apollo.pojo.ApolloMessagePojo;
import pl.pmajewski.ragen.clientAlpha.apollo.storage.ParticipantStorage;
import pl.pmajewski.ragen.clientAlpha.apollo.view.ErrorLayer;
import pl.pmajewski.ragen.clientAlpha.apollo.view.WaitLayer;
import pl.pmajewski.ragen.clientAlpha.apollo.view.fxml.ApolloConversationPresenter;
import pl.pmajewski.ragen.clientAlpha.stomp.response.ChatResponse;
import pl.pmajewski.ragen.clientAlpha.storage.ConnectorStorage;
import pl.pmajewski.ragen.clientAlpha.storage.DeviceAuthorizationStorage;

public class ApolloConversationService {

    private DeviceAuthorizationStorage deviceStorage = DeviceAuthorizationStorage.getInstance();
    private ConnectorStorage connectorStorage = ConnectorStorage.getInstance();
    private ParticipantStorage participantStorage = ParticipantStorage.getInstance();

    private ApolloConversationPresenter apolloConversation;

    public ApolloConversationService(ApolloConversationPresenter apolloConversation) {
        System.out.println("ApolloConversationService -> constructor()");
        this.apolloConversation = apolloConversation;
        init();
    }

    public void init() {
        apolloConversation.setState(ConversationState.SEARCHING);

        connectorStorage.getClient().subscribe("/queue/apollo/"+deviceStorage.getId()+"/messages", new TypeToken<ChatResponse<ApolloMessagePojo>>(){}.getType(), payload -> {
            Platform.runLater(() -> {
                ChatResponse<ApolloMessagePojo> message = (ChatResponse<ApolloMessagePojo>) payload;
                apolloConversation.addMessage(message.getContent());
            });
        });

        connectorStorage.getClient().subscribe("/queue/apollo/"+deviceStorage.getId()+"/participant/start", ChatResponse.class, payload -> {
            Platform.runLater(() -> {
                apolloConversation.setState(ConversationState.CONNECTED);
                apolloConversation.getActiveLayer().hide();
            });
        });

        connectorStorage.getClient().subscribe("/queue/apollo/"+deviceStorage.getId()+"/error", ChatResponse.class, payload -> {
            Platform.runLater(() -> {
                ChatResponse response = (ChatResponse) payload;
                apolloConversation.setState(ConversationState.EMPTY);
                ErrorLayer errorLayer = new ErrorLayer(response.getMessage());
                Button btn = new Button("Search Again");
                btn.setOnAction(e -> {
                    Platform.runLater(() -> {
                        apolloConversation.getActiveLayer().hide();
                        apolloConversation.setActiveLayer(new WaitLayer());
                        apolloConversation.getActiveLayer().show();
                        sendPreferencesRequest();
                    });
                });
                errorLayer.setButton(btn);
                apolloConversation.setActiveLayer(errorLayer);
                apolloConversation.getActiveLayer().show();
            });
        });

        sendPreferencesRequest();
    }

    private void sendPreferencesRequest() {
        connectorStorage.getClient().send("/app/apollo/participant/search", participantStorage.getRandomChatInitRequest());
    }
}
