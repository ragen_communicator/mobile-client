package pl.pmajewski.ragen.clientAlpha;

import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.visual.Swatch;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import pl.pmajewski.ragen.clientAlpha.initializer.view.LoadApplicationView;
import pl.pmajewski.ragen.clientAlpha.views.MainMenuView;

public class RagenClientAlpha extends MobileApplication {



    @Override
    public void init() {
        GlobalProperties.LOAD_APP_VIEW = this.HOME_VIEW;
        addViewFactory(GlobalProperties.LOAD_APP_VIEW, LoadApplicationView::new);
        addViewFactory(GlobalProperties.MAIN_MENU_VIEW, MainMenuView::new);
        //DrawerManager.buildDrawer(this);
    }

    @Override
    public void postInit(Scene scene) {
        System.out.println("RagenClientAlpha -> postInit()");
        Swatch.BLUE.assignTo(scene);
        this.getAppBar().setVisible(false);
        scene.getStylesheets().add(RagenClientAlpha.class.getResource("style.css").toExternalForm());
        ((Stage) scene.getWindow()).getIcons().add(new Image(RagenClientAlpha.class.getResourceAsStream("/icon.png")));
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        System.out.println("stop 1");
        Platform.exit();
        System.out.println("stop 3");
        System.exit(0);
    }
}
