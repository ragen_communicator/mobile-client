package pl.pmajewski.ragen.clientAlpha.stomp.client.impl;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.pmajewski.ragen.clientAlpha.stomp.client.StompSubscription;
import pl.pmajewski.ragen.clientAlpha.stomp.client.exception.DestinationSubscribedException;
import pl.pmajewski.ragen.clientAlpha.stomp.client.utils.ProtocolUtils;
import pl.pmajewski.ragen.clientAlpha.stomp.model.Subscription;

public class Subscriptions {
	
	private Integer subSequence = 0;
	private Map<String, Subscription> subscriptions = new HashMap<>();
	private Boolean allowOverrideSubscription = false;

	public Subscriptions() { }

	public Subscription addSubscription(String destination, Type responseType, StompSubscription subscription) {
		String fixedDestination = fix(destination);
		Subscription sub = new Subscription(subSequence++, fixedDestination, responseType, subscription);
		
		if(subscriptions.get(fixedDestination) != null && !this.allowOverrideSubscription) {
			throw new DestinationSubscribedException();
		} else {
			subscriptions.put(fixedDestination, sub);
			return sub;
		}
	}
	
	public void removeSubscription(String destination) {
		subscriptions.remove(fix(destination));
	}
	
	public Subscription getSubscription(String destination) {
		return subscriptions.get(fix(destination));
	}
	
	public List<Subscription> listAll() {
		List<Subscription> out = new ArrayList<>();
		
		subscriptions.entrySet().forEach(i -> {
			out.add(i.getValue());
		});
		
		return out;
	}
	
	private String fix(String destination) {
		return ProtocolUtils.destinationFormatter(destination);
	}

	public void setAllowOverrideSubscription(Boolean allowOverrideSubscription) {
		this.allowOverrideSubscription = allowOverrideSubscription;
	}
}
