package pl.pmajewski.ragen.clientAlpha.stomp.client.exception;

public class StompClientException extends RuntimeException {

	public StompClientException() {
		super();
	}

	public StompClientException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public StompClientException(String message, Throwable cause) {
		super(message, cause);
	}

	public StompClientException(String message) {
		super(message);
	}

	public StompClientException(Throwable cause) {
		super(cause);
	}
}
