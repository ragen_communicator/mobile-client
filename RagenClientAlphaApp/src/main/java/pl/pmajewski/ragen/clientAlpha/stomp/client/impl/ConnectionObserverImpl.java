package pl.pmajewski.ragen.clientAlpha.stomp.client.impl;

import pl.pmajewski.ragen.clientAlpha.stomp.client.ConnectionObserver;

public class ConnectionObserverImpl implements ConnectionObserver {
	
	private Boolean connected = false;
	
	@Override
	public void connectionOpened() {
		this.connected = true;
	}

	@Override
	public void connectionClosed() {
		this.connected = false;
	}

	public Boolean isConnected() {
		return connected;
	}
}
