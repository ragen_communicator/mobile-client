package pl.pmajewski.ragen.clientAlpha.apollo.view.fxml;

import com.gluonhq.charm.glisten.animation.BounceInRightTransition;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.layout.Layer;
import com.gluonhq.charm.glisten.mvc.View;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import lombok.Getter;
import lombok.Setter;
import pl.pmajewski.ragen.clientAlpha.apollo.ConversationState;
import pl.pmajewski.ragen.clientAlpha.apollo.pojo.ApolloMessagePojo;
import pl.pmajewski.ragen.clientAlpha.apollo.service.ApolloConversationService;
import pl.pmajewski.ragen.clientAlpha.apollo.storage.ParticipantStorage;
import pl.pmajewski.ragen.clientAlpha.apollo.view.MessageBubble;
import pl.pmajewski.ragen.clientAlpha.apollo.view.WaitLayer;
import pl.pmajewski.ragen.clientAlpha.storage.ConnectorStorage;
import pl.pmajewski.ragen.clientAlpha.storage.DeviceAuthorizationStorage;

import java.time.LocalDateTime;

@Getter
@Setter
public class ApolloConversationPresenter {

    @FXML
    private View root;
    @FXML
    private TextField contentInput;
    @FXML
    private Button sendButton;
    @FXML
    private VBox messagesContainer;
    @FXML
    private ScrollPane scrollPane;

    private DeviceAuthorizationStorage deviceStorage = DeviceAuthorizationStorage.getInstance();
    private ConnectorStorage connectorStorage = ConnectorStorage.getInstance();
    private ParticipantStorage participantStorage = ParticipantStorage.getInstance();

    private Layer activeLayer;
    private ConversationState state;

    private ApolloConversationService apolloConversationService;

    public void initialize() {
        root.showTransitionFactoryProperty().setValue(t -> {
            return new BounceInRightTransition(MobileApplication.getInstance().getView());
        });

        activeLayer = new WaitLayer();

        root.setOnShowing(e -> {
            root.getApplication().getAppBar().setVisible(false);
            activeLayer.show();
        });

        root.setOnShown(e -> {
            apolloConversationService = new ApolloConversationService(this);
        });

        root.setOnCloseRequest(e -> {
            connectorStorage.getClient().send("/app/apollo/participant/cancel", "");
        });

        messagesContainer.heightProperty().addListener((observable, oldValue, newValue) -> {
            scrollPane.setVvalue((Double)newValue);
        });
    }

    public void onSendButton() {
        ApolloMessagePojo message = new ApolloMessagePojo();
        message.setDate(LocalDateTime.now());
        message.setContent(contentInput.getText());
        connectorStorage.getClient().send("/app/apollo/participant/messages/send", message);
        contentInput.setText("");
        addMessageRight(message);
    }

    public void onEnterPressed(KeyEvent event) {
        if(event.getCode().equals(KeyCode.ENTER)) {
            onSendButton();
        }
    }

    public void addMessage(ApolloMessagePojo message) {
        addMessageLeft(message);
    }

    private void addMessageLeft(ApolloMessagePojo message) {
        MessageBubble bubble = new MessageBubble(message, MessageBubble.Position.LEFT);
        messagesContainer.getChildren().add(bubble);
    }

    private void addMessageRight(ApolloMessagePojo message) {
        MessageBubble bubble = new MessageBubble(message, MessageBubble.Position.RIGHT);
        messagesContainer.getChildren().add(bubble);
    }
}
