package pl.pmajewski.ragen.clientAlpha.initializer.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DeviceConnectAuthorizationRequestPojo {

    private String deviceId;
}
