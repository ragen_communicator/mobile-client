package pl.pmajewski.ragen.clientAlpha.stomp.model;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class StompHeaders {
	
	private Map<StompHeader, String> headers = new HashMap<>();
	
	public StompHeaders() { }
	
	public StompHeaders(Map<StompHeader, String> headers) { 
		this.headers = headers;
	}
	
	public void add(StompHeader header, String value) {
		this.headers.put(header, value);
	}

	public void add(Map<StompHeader, String> headers) {
		this.headers.putAll(headers);
	}
	
	public void remove(StompHeader header) {
		this.headers.remove(header);
	}
	
	public String getHeaderValue(StompHeader header) {
		return headers.get(header);
	}
	
	public Map<StompHeader, String> getHeaders() {
		return headers;
	}
	
	public static StompHeaders prepareFromPayload(String payload) {
		StompHeaders out = new StompHeaders();
		
		String[] headers = payload.split("\n");
		Arrays.stream(headers).forEach(i -> {
			String[] entry = i.split(":");
			StompHeader header = StompHeader.fromString(entry[0]);
			out.add(header, entry[1]);
		});
		
		return out;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		for (Map.Entry<StompHeader, String> header: headers.entrySet()) {
			sb.append(header.getKey().toString())
			.append(":")
			.append(header.getValue())
			.append("\n");
		}
		
		return sb.toString();
	};
}
