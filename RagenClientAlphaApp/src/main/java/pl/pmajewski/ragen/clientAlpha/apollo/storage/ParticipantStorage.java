package pl.pmajewski.ragen.clientAlpha.apollo.storage;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.pmajewski.ragen.clientAlpha.apollo.pojo.RandomChatInitRequest;
import pl.pmajewski.ragen.clientAlpha.utils.Sex;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@Setter
public class ParticipantStorage {

    private static ParticipantStorage instance;

    private Sex userSex;
    private Sex preferedSex;

    public static ParticipantStorage getInstance() {
        if(instance == null) {
            instance = new ParticipantStorage();
        }
        return instance;
    }

    public static boolean isNullInstance() {
        return instance == null;
    }

    public RandomChatInitRequest getRandomChatInitRequest() {
        RandomChatInitRequest request = new RandomChatInitRequest();
        request.setSex(userSex);
        request.setPreferedSex(preferedSex);
        return request;
    }
}
