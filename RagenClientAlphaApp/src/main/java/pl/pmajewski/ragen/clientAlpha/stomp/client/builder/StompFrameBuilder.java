package pl.pmajewski.ragen.clientAlpha.stomp.client.builder;

import pl.pmajewski.ragen.clientAlpha.stomp.model.*;

import java.util.Map;

public class StompFrameBuilder {
	
	public static StompFrame prepareConnectFrame() {
		StompHeaders headers = new StompHeaders();
		headers.add(StompHeader.ACCEPT_VERSION, "1.0,1.1,1.2,2.0");
		headers.add(StompHeader.HEART_BEAT, "0,0");
		
		return new StompFrame(StompCommand.CONNECT, headers);
	}
	
	public static StompFrame prepareSubscribeFrame(Subscription subscription) {
		StompHeaders headers = new StompHeaders();
		headers.add(StompHeader.ID, subscription.getId().toString());
		headers.add(StompHeader.DESTINATION, subscription.getDestination());
		headers.add(StompHeader.ACK, "auto");
		
		return new StompFrame(StompCommand.SUBSCRIBE, headers);
	}
	
	public static StompFrame prepareUnsubscribeFrame(Subscription subscription) {
		StompHeaders headers = new StompHeaders();
		headers.add(StompHeader.ID, subscription.getId().toString());
		
		return new StompFrame(StompCommand.UNSUBSCRIBE, headers);
	}
	
	public static StompFrame prepareSendFrame(String destination, String payload) {
		StompHeaders headers = new StompHeaders();
		headers.add(StompHeader.DESTINATION, destination);

		return new StompFrame(StompCommand.SEND, headers, payload);
	}

	public static StompFrame prepareSendFrame(String destination, Map<StompHeader, String> additionalHeaders, String payload) {
		StompHeaders headers = new StompHeaders();
		headers.add(StompHeader.DESTINATION, destination);

		return new StompFrame(StompCommand.SEND, headers, payload);
	}
}
