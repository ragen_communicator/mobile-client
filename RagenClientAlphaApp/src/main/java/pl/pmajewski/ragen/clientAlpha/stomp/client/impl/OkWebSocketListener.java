package pl.pmajewski.ragen.clientAlpha.stomp.client.impl;

import com.fatboyindustrial.gsonjavatime.Converters;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;
import pl.pmajewski.ragen.clientAlpha.stomp.client.ConnectionObservable;
import pl.pmajewski.ragen.clientAlpha.stomp.client.ConnectionObserver;
import pl.pmajewski.ragen.clientAlpha.stomp.client.builder.StompFrameBuilder;
import pl.pmajewski.ragen.clientAlpha.stomp.model.StompCommand;
import pl.pmajewski.ragen.clientAlpha.stomp.model.StompFrame;
import pl.pmajewski.ragen.clientAlpha.stomp.model.StompHeader;
import pl.pmajewski.ragen.clientAlpha.stomp.model.Subscription;

import java.util.ArrayList;
import java.util.List;

public class OkWebSocketListener extends WebSocketListener implements ConnectionObservable {

	private WebSocket stompWebSocket;
	private List<ConnectionObserver> connectionObservers = new ArrayList<>();
	private Subscriptions subscriptions;
	private Gson gson;
	private Boolean debugMode = false;
	
	public OkWebSocketListener() {
		initGson();
	}

	private void initGson() {
        GsonBuilder builder = new GsonBuilder();
        Converters.registerLocalDateTime(builder);
        builder.setDateFormat("dd-MM-yyyy HH:mm:ss");
        this.gson = builder.create();
    }
	
	@Override
	public void onOpen(WebSocket webSocket, Response response) {
		this.stompWebSocket = webSocket;
		write(StompFrameBuilder.prepareConnectFrame().toString());
	}

	@Override
	public void onMessage(WebSocket webSocket, ByteString bytes) {
		super.onMessage(webSocket, bytes);
	}

	@Override
	public void onMessage(WebSocket webSocket, String text) {
		try {
			StompFrame frame = StompFrame.prepareFromPayload(text);
			if(debugMode) {
				System.out.println("======== Handle Frame ========");
				System.out.println(frame.toString());
				System.out.println("==============================");
			}
			
			if(StompCommand.CONNECTED.equals(frame.getCommand())) {
				notifyOpened();			
			} else if(StompCommand.MESSAGE.equals(frame.getCommand())) {
				Subscription subscription = subscriptions.getSubscription(frame.getHeader(StompHeader.DESTINATION));
				subscription.getSubscription().onMessage(gson.fromJson(frame.getBody(), subscription.getResponseType()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void write(String frame) {
		this.stompWebSocket.send(frame);
	}
	
	@Override
	public void onClosed(WebSocket webSocket, int code, String reason) {
		System.out.println("OkWebSocketListener -> onClose()");
		notifyClosed();
	}

	@Override
	public void addObserver(ConnectionObserver observer) {
		connectionObservers.add(observer);
	}

	@Override
	public void removeObserver(ConnectionObserver observer) {
		connectionObservers.remove(observer);
	}
	
	private void notifyOpened() {
		connectionObservers.forEach(i -> {
			i.connectionOpened();
		});
	}
	
	private void notifyClosed() {
		connectionObservers.forEach(i -> {
			i.connectionClosed();
		});
	}

	public void setSubscriptions(Subscriptions subscriptions) {
		this.subscriptions = subscriptions;
	}

	public void setDebugMode(Boolean debugMode) {
		this.debugMode = debugMode;
	}
}
