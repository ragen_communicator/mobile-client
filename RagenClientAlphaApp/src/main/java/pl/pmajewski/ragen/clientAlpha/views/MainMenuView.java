package pl.pmajewski.ragen.clientAlpha.views;

import com.gluonhq.charm.glisten.animation.BounceInRightTransition;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.control.AppBar;
import com.gluonhq.charm.glisten.mvc.View;
import javafx.application.Platform;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import pl.pmajewski.ragen.clientAlpha.GlobalProperties;
import pl.pmajewski.ragen.clientAlpha.RagenClientAlpha;
import pl.pmajewski.ragen.clientAlpha.apollo.storage.ParticipantStorage;
import pl.pmajewski.ragen.clientAlpha.apollo.view.fxml.ApolloConversationView;
import pl.pmajewski.ragen.clientAlpha.apollo.view.fxml.ParticipantSelectorView;
import pl.pmajewski.ragen.clientAlpha.storage.ConnectorStorage;

public class MainMenuView extends View {

    private ConnectorStorage connectorStorage = ConnectorStorage.getInstance();

    public MainMenuView() {
        init();
        getStylesheets().add(RagenClientAlpha.class.getResource("style.css").toExternalForm());
        GridPane gridPane = prepareGrindPane();

        Button registerBtn = new Button();
        registerBtn.setText("Register");
        registerBtn.getStyleClass().add("register_btn");
        registerBtn.setPrefWidth(GlobalProperties.getScreenWidth()*(8d/10d));
        registerBtn.setPrefHeight(GlobalProperties.getScreenHeight()*(1d/10d));
        gridPane.add(registerBtn,1,1);

        Button randomChatBtn = new Button();
        randomChatBtn.setText("Random");
        randomChatBtn.getStyleClass().add("random_btn");
        randomChatBtn.setPrefWidth(GlobalProperties.getScreenWidth()*(50d/100d));
        randomChatBtn.setPrefHeight(GlobalProperties.getScreenHeight()*(30d/100d));
        randomChatBtn.setOnAction(e -> {
            switchRandomConversationView();
        });
        gridPane.add(randomChatBtn, 1,3);

        Label responseLbl = new Label();
        gridPane.add(responseLbl, 1, 0);

        setCenter(gridPane);

        this.showTransitionFactoryProperty().setValue(t -> {
            return new BounceInRightTransition(MobileApplication.getInstance().getView());
        });

        this.setOnCloseRequest(e -> {
           //System.exit(0);
        });
    }

    private void init() {
        MobileApplication.getInstance().getAppBar().setVisible(false);
    }

    private GridPane prepareGrindPane() {
        GridPane gridPane = new GridPane();
        gridPane.getStyleClass().add("login_bgn");

        gridPane.setMaxWidth(GlobalProperties.getScreenWidth());
        gridPane.setMaxHeight(GlobalProperties.getScreenHeight());
        gridPane.prefWidth(GlobalProperties.getScreenWidth());
        gridPane.prefHeight(GlobalProperties.getScreenHeight());
        gridPane.setAlignment(Pos.CENTER);

        addColConstraints(gridPane);
        addRowConstraints(gridPane);

        return gridPane;
    }

    private void addRowConstraints(GridPane gridPane) {
        RowConstraints topRow = new RowConstraints(); // 0
        topRow.setPercentHeight(30);

        RowConstraints registerRow = new RowConstraints(); // 1
        registerRow.setPercentHeight(10);

        RowConstraints spliterRow = new RowConstraints(); // 2
        spliterRow.setPercentHeight(10);

        RowConstraints randomChatRow = new RowConstraints(); // 3
        randomChatRow.setPercentHeight(10);

        RowConstraints bottomChatRow = new RowConstraints(); // 4
        bottomChatRow.setPercentHeight(40);

        gridPane.getRowConstraints().addAll(topRow, registerRow, spliterRow, randomChatRow, bottomChatRow);
    }

    @Override
    protected void updateAppBar(AppBar appBar) {
        super.updateAppBar(appBar);
        System.out.println("MainMenuView -> updateAppBar()");
        appBar.setVisible(false);
    }

    private void switchRandomConversationView() {
        if(!ParticipantStorage.isNullInstance()) {
            Platform.runLater(() -> {
                MobileApplication.getInstance().addViewFactory(GlobalProperties.RANDOM_CONVERSATION_VIEW,  () -> new ApolloConversationView().getView());
                MobileApplication.getInstance().switchView(GlobalProperties.RANDOM_CONVERSATION_VIEW);
            });
        } else {
            Platform.runLater(() -> {
                MobileApplication.getInstance().addViewFactory(GlobalProperties.PARTICIPANT_SELECTOR_VIEW, () -> new ParticipantSelectorView().getView());
                MobileApplication.getInstance().switchView(GlobalProperties.PARTICIPANT_SELECTOR_VIEW);
            });
        }


    }

    private void addColConstraints(GridPane gridPane) {
        ColumnConstraints leftCol = new ColumnConstraints();
        leftCol.setPercentWidth(10);

        ColumnConstraints rightCol = new ColumnConstraints();
        rightCol.setPercentWidth(10);

        ColumnConstraints centerCol = new ColumnConstraints();
        centerCol.setPercentWidth(80);
        centerCol.setHalignment(HPos.CENTER);

        gridPane.getColumnConstraints().addAll(leftCol, centerCol, rightCol);
    }
}
