package pl.pmajewski.ragen.clientAlpha.stomp.client;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import pl.pmajewski.ragen.clientAlpha.stomp.model.StompHeader;
import pl.pmajewski.ragen.clientAlpha.stomp.model.Subscription;

public interface StompClient {
	
	void send(String destination, Object body);

	public void send(String destination, Map<StompHeader, String> headers, Object body);
	
	void subscribe(String destination, Type responseType, StompSubscription onMessage);
	
	List<Subscription> listSubscriptions();
	
	void unsubscribe(String destination);

	void connect();

	boolean isConnected();
}
