package pl.pmajewski.ragen.clientAlpha.apollo;

public enum ConversationState {
    CONNECTED, SEARCHING, EMPTY;
}
