package pl.pmajewski.ragen.clientAlpha.stomp.client.exception;

public class ClientNotConnectedException extends StompClientException {

	private static final long serialVersionUID = 1L;

	public ClientNotConnectedException() {
		super();
	}

	public ClientNotConnectedException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ClientNotConnectedException(String message, Throwable cause) {
		super(message, cause);
	}

	public ClientNotConnectedException(String message) {
		super(message);
	}

	public ClientNotConnectedException(Throwable cause) {
		super(cause);
	}
}
