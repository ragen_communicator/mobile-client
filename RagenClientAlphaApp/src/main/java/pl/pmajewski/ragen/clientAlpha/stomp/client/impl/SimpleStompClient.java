package pl.pmajewski.ragen.clientAlpha.stomp.client.impl;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import com.fatboyindustrial.gsonjavatime.Converters;
import com.google.gson.*;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.WebSocket;
import pl.pmajewski.ragen.clientAlpha.stomp.client.ConnectionObserver;
import pl.pmajewski.ragen.clientAlpha.stomp.client.StompClient;
import pl.pmajewski.ragen.clientAlpha.stomp.client.StompSubscription;
import pl.pmajewski.ragen.clientAlpha.stomp.client.builder.StompFrameBuilder;
import pl.pmajewski.ragen.clientAlpha.stomp.client.exception.ClientNotConnectedException;
import pl.pmajewski.ragen.clientAlpha.stomp.model.StompFrame;
import pl.pmajewski.ragen.clientAlpha.stomp.model.StompHeader;
import pl.pmajewski.ragen.clientAlpha.stomp.model.Subscription;

public class SimpleStompClient implements StompClient {

	// SINGLETON
	private static SimpleStompClient instance;

	// OKHTTP
	private OkHttpClient client;
	private WebSocket webSocket;
	private String endpointUrl;

	// LISTENER
	private OkWebSocketListener wsListener;

	// INTERNAL MANAGEMENT
	private Subscriptions subscriptions = new Subscriptions();
	private ConnectionObserver connectionObserver;
	private Gson gson;
	private Boolean debugMode = false;

	public SimpleStompClient(String url) {
		initListener();
		initGson();
		this.client = new OkHttpClient();
		this.endpointUrl = url;
	}

	@Override
	public void connect() {
		this.webSocket = client.newWebSocket(getRequest(endpointUrl), wsListener);
	}

	private Request getRequest(String url) {
		return new Request.Builder().url(url).build();
	}

	public void send(String payload) {
		webSocket.send(payload);
	}

	public void close() {
		webSocket.close(1000, "Normal Closure");
	}
	
	private void initListener() {
		this.connectionObserver = new ConnectionObserverImpl();
		this.wsListener = new OkWebSocketListener();
		this.wsListener.addObserver(connectionObserver);
		this.wsListener.setSubscriptions(subscriptions);
	}
	
	private void initGson() {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.setDateFormat("dd-MM-yyyy HH:mm:ss");
		Converters.registerLocalDateTime(gsonBuilder);
		this.gson = gsonBuilder.create();
	}

	@Override
	public void send(String destination, Object body) {
		StompFrame frame = StompFrameBuilder.prepareSendFrame(destination, gson.toJson(body));
		if(debugMode) {
            System.out.println(frame);
        }
		webSocket.send(frame.toString());
	}

    @Override
    public void send(String destination, Map<StompHeader, String> headers, Object body) {
        StompFrame frame = StompFrameBuilder.prepareSendFrame(destination, headers, gson.toJson(body));
        if(debugMode) {
            System.out.println(frame);
        }
        webSocket.send(frame.toString());
    }

	@Override
	public void subscribe(String destination, Type responseType, StompSubscription onMessage) {
		if(connectionObserver.isConnected()) {
			Subscription subscription = subscriptions.addSubscription(destination, responseType, onMessage);
			webSocket.send(StompFrameBuilder.prepareSubscribeFrame(subscription).toString());
		} else {
			throw new ClientNotConnectedException();
		}
	}

	@Override
	public List<Subscription> listSubscriptions() {
		return subscriptions.listAll();
	}

	@Override
	public void unsubscribe(String destination) {
		Subscription subscription = subscriptions.getSubscription(destination);
		webSocket.send(StompFrameBuilder.prepareUnsubscribeFrame(subscription).toString());
		subscriptions.removeSubscription(destination);
	}

	public void setDebugMode(Boolean debugMode) {
	    this.debugMode = debugMode;
		wsListener.setDebugMode(debugMode);
	}

	@Override
	public boolean isConnected() {
		return connectionObserver.isConnected();
	}

	public void allowOverrideSubscription(Boolean value) {
        subscriptions.setAllowOverrideSubscription(value);
    }
}