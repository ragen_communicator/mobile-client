package pl.pmajewski.ragen.clientAlpha.stomp.response;

import java.util.Calendar;

public class MasterResponse {

    private Long userId;
    private Calendar time;
    private String messageContent;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Calendar getTime() {
        return time;
    }

    public void setTime(Calendar time) {
        this.time = time;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }
}
