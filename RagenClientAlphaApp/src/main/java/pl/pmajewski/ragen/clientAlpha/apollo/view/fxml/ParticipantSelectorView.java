package pl.pmajewski.ragen.clientAlpha.apollo.view.fxml;

import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.mvc.View;
import javafx.fxml.FXMLLoader;

import java.io.IOException;

public class ParticipantSelectorView {

    private View view;

    public ParticipantSelectorView() {
        try {
            view = FXMLLoader.load(ParticipantSelectorView.class.getResource("ParticipantSelector.fxml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public View getView() {
        return view;
    }
}
