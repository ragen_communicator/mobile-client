package pl.pmajewski.ragen.clientAlpha.utils;

public enum Sex {
    MALE, FEMALE, ANY;
}
