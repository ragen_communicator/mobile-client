package pl.pmajewski.ragen.clientAlpha.stomp.model;

public enum StompCommand {
	CONNECT,
	CONNECTED,
	SEND,
	SUBSCRIBE,
	UNSUBSCRIBE,
	ACK,
	NACK,
	BEGIN,
	COMMIT,
	ABORT,
	DISCONNECT,
	MESSAGE,
	ERROR
}
