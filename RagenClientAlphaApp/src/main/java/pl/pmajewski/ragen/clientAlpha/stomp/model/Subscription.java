package pl.pmajewski.ragen.clientAlpha.stomp.model;

import pl.pmajewski.ragen.clientAlpha.stomp.client.StompSubscription;

import java.lang.reflect.Type;

public class Subscription {
	
	private Integer id;
	private String destination;
	private Type responseType;
	private StompSubscription subscription;

	public Subscription(Integer id, String destination, Type responseType, StompSubscription subscription) {
		this.id = id;
		this.destination = destination;
		this.responseType = responseType;
		this.subscription = subscription;
	}
	
	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public StompSubscription getSubscription() {
		return subscription;
	}

	public void setSubscription(StompSubscription subscription) {
		this.subscription = subscription;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Type getResponseType() {
		return responseType;
	}

	public void setResponseType(Class responseType) {
		this.responseType = responseType;
	}

	@Override
	public int hashCode() {
		return destination.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof Subscription)) {
			return false;
		}
		
		Subscription sub = (Subscription) obj;
		if(sub.getDestination().equalsIgnoreCase(destination)) {
			return true;
		} else {
			return false;
		}
	}
}
