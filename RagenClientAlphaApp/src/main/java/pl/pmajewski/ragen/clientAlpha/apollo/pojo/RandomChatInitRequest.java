package pl.pmajewski.ragen.clientAlpha.apollo.pojo;

import lombok.Data;
import pl.pmajewski.ragen.clientAlpha.utils.Sex;

@Data
public class RandomChatInitRequest {

    private Sex sex;
    private String longitude;
    private String latitude;
    private Sex preferedSex;
}
