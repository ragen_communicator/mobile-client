package pl.pmajewski.ragen.clientAlpha.stomp.model;

public class StompFrame {
	
	private StompCommand command;
	private StompHeaders headers = new StompHeaders();
	private String body;
	
	public StompFrame(StompCommand command) {
		this.command = command;
	}
	
	public StompFrame(StompCommand command, StompHeaders headers) {
		this.command = command;
		this.headers = headers;
	}
	
	public StompFrame(StompCommand command, StompHeaders headers, String body) {
		this.command = command;
		this.headers = headers;
		this.body = body;
	}
	
	public StompCommand getCommand() {
		return command;
	}
	
	public void addHeader(StompHeader header, String value) {
		headers.add(header, value);
	}
	
	public void setBody(String body) {
		this.body = body;
	}
	
	public static StompFrame prepareFromPayload(String payload) {
		String commandStr = payload.substring(0, payload.indexOf("\n"));
		StompCommand command = StompCommand.valueOf(commandStr);
		String headers = payload.substring(payload.indexOf("\n")+1, payload.indexOf("\n\n"));
		
		StompFrame frame = null;
		if(StompCommand.SEND.equals(command) || StompCommand.MESSAGE.equals(command) || StompCommand.ERROR.equals(command)) {
			frame = new StompFrame(command, StompHeaders.prepareFromPayload(headers), payload.substring(payload.indexOf("\n\n"), payload.length()-1));
		} else {
			frame = new StompFrame(command, StompHeaders.prepareFromPayload(headers));
		}
		
		return frame;
	}
	
	public String getHeader(StompHeader header) {
		return headers.getHeaderValue(header);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append(command.toString()).append("\n");
		sb.append(headers.toString()).append("\n");
		
		if(body != null) {
			sb.append(body)
			.append("\n\n\u0000");
		} else {
			sb.append("\u0000");
		}
		
		return sb.toString();
	}

	public String getBody() {
		return body;
	}
}
