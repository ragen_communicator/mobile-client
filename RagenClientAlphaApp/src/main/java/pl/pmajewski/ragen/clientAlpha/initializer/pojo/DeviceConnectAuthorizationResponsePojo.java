package pl.pmajewski.ragen.clientAlpha.initializer.pojo;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class DeviceConnectAuthorizationResponsePojo {

    private Long id;
}
