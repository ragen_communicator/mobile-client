package pl.pmajewski.ragen.clientAlpha.storage;

import lombok.Getter;
import pl.pmajewski.ragen.clientAlpha.stomp.client.StompClient;
import pl.pmajewski.ragen.clientAlpha.stomp.client.impl.SimpleStompClient;

@Getter
public class ConnectorStorage {

    private StompClient client;

    private static ConnectorStorage instance;

    private ConnectorStorage()  {
        init();
    }

    public static ConnectorStorage getInstance() {
        if(instance == null) {
            instance = new ConnectorStorage();
        }

        return instance;
    }

    private void init() {
        SimpleStompClient client = new SimpleStompClient("ws://192.168.0.72:8081/chat");
        client.setDebugMode(true);
        client.allowOverrideSubscription(true);
        this.client = client;
    }

    private  void connect() {
        this.client.connect();
    }
}
