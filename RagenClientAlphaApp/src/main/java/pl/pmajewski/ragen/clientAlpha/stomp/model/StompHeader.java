package pl.pmajewski.ragen.clientAlpha.stomp.model;

public enum StompHeader {
	DESTINATION("destination"),
	CONTENT_TYPE("content-type"),
	ID("id"),
	ACK("ack"),
	HEART_BEAT("heart-beat"),
	ACCEPT_VERSION("accept-version"),
	VERSION("version"),
	HOST("host"),
	RECEIPT("receipt"),
	SUBSCRIPTION_ID("subscription"),
	MESSAGE_ID("message-id"),
	CONTENT_LENGTH("content-length"),
    USER("user"),
	USERNAME("user-name");

	private String value;
	
	private StompHeader(String value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return this.value;
	}
	
	public static StompHeader fromString(String value) {
		for (StompHeader temp : StompHeader.values()) {
			if(temp.toString().equals(value)) {
				return temp;
			}
		};
		
		throw new IllegalArgumentException("Illeagal command: "+value);
	}
}
