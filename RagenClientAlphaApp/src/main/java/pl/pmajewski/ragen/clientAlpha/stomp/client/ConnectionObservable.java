package pl.pmajewski.ragen.clientAlpha.stomp.client;

public interface ConnectionObservable {

	void addObserver(ConnectionObserver observer);
	
	void removeObserver(ConnectionObserver observer);
}
