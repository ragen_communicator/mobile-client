package pl.pmajewski.ragen.clientAlpha.apollo.view;

import com.gluonhq.charm.glisten.application.GlassPane;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.layout.Layer;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import lombok.Setter;

public class ErrorLayer extends Layer {

    private final Node root;
    private final double size = 150;
    private String message;
    @Setter
    private Button button;
    final GlassPane glassPane = MobileApplication.getInstance().getGlassPane();

    public ErrorLayer(String message) {
        this.message = message;
        setAutoHide(false);
        setBackgroundFade(0.5);
        root = new StackPane(new Label(this.message));
        root.setStyle("-fx-background-color: white;");
        getChildren().add(root);

        setOnShowing(e -> {
            if(button != null) {
                if(!((StackPane) root).getChildren().contains(button)) {
                    ((StackPane) root).getChildren().add(button);
                }
            }
        });
    }

    @Override
    public void layoutChildren() {
        super.layoutChildren();
        root.setVisible(isShowing());
        if (!isShowing()) {
            return;
        }
        root.resize(size, size);
        resizeRelocate((glassPane.getWidth() - size)/2, (glassPane.getHeight()- size)/2, size, size);
    }
}
