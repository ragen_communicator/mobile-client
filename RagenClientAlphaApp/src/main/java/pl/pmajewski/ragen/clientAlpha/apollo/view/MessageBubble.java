package pl.pmajewski.ragen.clientAlpha.apollo.view;

import com.gluonhq.charm.glisten.application.MobileApplication;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import pl.pmajewski.ragen.clientAlpha.GlobalProperties;
import pl.pmajewski.ragen.clientAlpha.RagenClientAlpha;
import pl.pmajewski.ragen.clientAlpha.apollo.pojo.ApolloMessagePojo;
import pl.pmajewski.ragen.clientAlpha.views.MainMenuView;

public class MessageBubble extends VBox {

    public enum Position {LEFT, RIGHT}
    Label label = new Label();

    public MessageBubble(ApolloMessagePojo message) {
        super();

        label.getStylesheets().add(RagenClientAlpha.class.getResource("style.css").toExternalForm());
        label.getStyleClass().add("chat-bubble");
        label.setText(message.getContent());
        label.setMaxHeight(10000);
        label.setWrapText(true);

        this.setPrefWidth(GlobalProperties.getScreenWidth());
        this.getChildren().add(label);
        this.setPadding(new Insets(0, 6, 4, 6));
    }

    public MessageBubble(ApolloMessagePojo message, Position position) {
        this(message);
        if(Position.RIGHT.equals(position)) {
            setAlignment(Pos.CENTER_RIGHT);
            label.setAlignment(Pos.CENTER_RIGHT);
            label.getStyleClass().add("chat-bubble-background-right");
        } else {
            setAlignment(Pos.CENTER_LEFT);
            label.setAlignment(Pos.CENTER_LEFT);
            label.getStyleClass().add("chat-bubble-background-left");
        }
    }
}
