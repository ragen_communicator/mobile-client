package pl.pmajewski.ragen.clientAlpha.apollo.view;

import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;
import pl.pmajewski.ragen.clientAlpha.GlobalProperties;
import pl.pmajewski.ragen.clientAlpha.RagenClientAlpha;
import pl.pmajewski.ragen.clientAlpha.apollo.pojo.ApolloMessagePojo;

public class MessageList extends ScrollPane {

    private VBox vBox = new VBox();

    public MessageList() {
        setContent(vBox);

        getStylesheets().add(RagenClientAlpha.class.getResource("style.css").toExternalForm());
        getStyleClass().add("conversation-background");
        prefWidth(GlobalProperties.getScreenWidth());

        vBox.getStylesheets().add(RagenClientAlpha.class.getResource("style.css").toExternalForm());
        vBox.getStyleClass().add("conversation-background");
        vBox.setPrefWidth(GlobalProperties.getScreenWidth());
    }

    public void addMessage(MessageBubble msg) {
        vBox.getChildren().add(msg);
    }

    public void addMessage(ApolloMessagePojo msg) {
        addMessage(msg, MessageBubble.Position.LEFT);
    }

    public void addMessage(ApolloMessagePojo msg, MessageBubble.Position position) {
        MessageBubble message = null;
        if(MessageBubble.Position.RIGHT.equals(position)) {
            message = new MessageBubble(msg, MessageBubble.Position.RIGHT);
        } else {
            message = new MessageBubble(msg, MessageBubble.Position.LEFT);
        }
        vBox.getChildren().add(message);
    }
}
