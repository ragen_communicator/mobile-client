package pl.pmajewski.ragen.clientAlpha.apollo;

public class RandomConversationService {

    private static RandomConversationService instance;

    private ConversationState state;

    public static RandomConversationService getInstance() {
        if(instance == null) {
            instance = new RandomConversationService();
        }

        return  instance;
    }
}
