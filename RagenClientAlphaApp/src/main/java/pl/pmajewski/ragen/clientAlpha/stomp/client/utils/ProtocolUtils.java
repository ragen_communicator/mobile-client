package pl.pmajewski.ragen.clientAlpha.stomp.client.utils;

public class ProtocolUtils {
	
	public static String destinationFormatter(String destination) {
		if(!destination.startsWith("/")) {
			destination = "/"+destination;
		}
		
		return destination;
	}
	
}
