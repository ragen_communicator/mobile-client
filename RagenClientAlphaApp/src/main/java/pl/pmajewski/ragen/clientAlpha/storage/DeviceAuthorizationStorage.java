package pl.pmajewski.ragen.clientAlpha.storage;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DeviceAuthorizationStorage {

    // SINGLETON
    private static DeviceAuthorizationStorage instance;

    private Long id;

    // SINGLETON
    public static DeviceAuthorizationStorage getInstance() {
        if(instance == null) {
            instance = new DeviceAuthorizationStorage();
        }
        return instance;
    }
}