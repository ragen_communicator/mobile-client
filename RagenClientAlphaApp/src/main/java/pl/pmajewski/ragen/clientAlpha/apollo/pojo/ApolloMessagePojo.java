package pl.pmajewski.ragen.clientAlpha.apollo.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@ToString
public class ApolloMessagePojo {

    private Long id;
    private String contentType;
    private String content;
    private LocalDateTime date;
}
